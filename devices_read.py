#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'yinzhuoqun'

# 日志级别等级 ERROR > WARNING > INFO > DEBUG 等几个级别
import logging

logging.basicConfig(level=logging.INFO)
# logging.basicConfig(level=logging.WARNING)

import xlrd, os, datetime


# 获取表格 bug 信息，第一个 sheet
def get_xlsx_info(path):
    if os.path.exists(path) == True:
        try:
            book = xlrd.open_workbook(path)  # 打开Excel文件读取数据
        except Exception as e:
            print('警告：文件打开失败，原因：%s ' % e)
        else:
            table = book.sheets()[2]  # 通过索引顺序获取sheet[0123]
            # print(table)
            nrows = table.nrows  # 获取行数
            ncols = table.ncols  # 获取列数
            contents = []
            content_one = {}
            book_datemode = book.datemode
            for row_order in range(4, 5):
                one = []
                for info in table.row(row_order):
                    if info.ctype == 3:
                        year, month, day, hour, minute, second = xlrd.xldate_as_tuple(info.value,
                                                                                      book.datemode)
                        info = datetime.datetime(year, month, day, hour, minute, second)
                    print(info)
                    # print(dir(info))

                #     one.append(info)
                # contents.append(one)

            # print(contents[4:], "\n")




if __name__ == "__main__":
    # path = r"I:\yzq\work\devices\测试设备详情.xlsx"
    # get_xlsx_info(path)

    # 1- 7+9+4
    for x in range(1,8):
        for y in range(1,10):
            for z in range(1,5):
                pass