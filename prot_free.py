#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'yinzhuoqun'

# 日志级别等级 ERROR > WARNING > INFO > DEBUG 等几个级别
import logging

logging.basicConfig(level=logging.INFO)
# logging.basicConfig(level=logging.WARNING)


import os

port_msg_input = "请输入停止的端口号:\n"
port_msg_err = "端口号格式为整数"
port_msg_notfind = "端口未使用"
prot_msg_stop_alert = "确定停止端口"


def port_stop():
    if os.name == "nt":
        port = input(port_msg_input).strip()
        while not port.isdigit():
            print(port_msg_err)
            port = input(port_msg_input).strip()

        # port = 8090

        # port_info = subprocess.check_output('netstat -ano|findstr "%s"' % port)
        port_info = os.popen('netstat -ano|findstr "%s"' % port).read()
        # port_info = os.system('netstat -ano|findstr "%s"' % port)
        print(port_info)
        if port_info:
            port_pid = port_info.split(" ")[-1].replace("\n", "")
            # print('PID：', port_pid)
            port_task = os.popen('tasklist|findstr "%s"' % port_pid).read()
            print(port_task)
            port_pid_reinfo = port_task.split(" ")
            for info in port_pid_reinfo:
                if len(info) == 0:
                    port_pid_reinfo.remove(info)
            # print(port_pid_reinfo)
            port_pid_r = port_pid_reinfo[1]
            if port_pid == port_pid_r:
                port_stop_alert = input("%s %s ? yes/q\n" % (prot_msg_stop_alert, port))
                if port_stop_alert.lower() in ["yes", "y"]:
                    port_stop = os.system("taskkill /f /pid %s" % port_pid)
                else:
                    os.system(exit())

        else:
            print("%s %s" % (port, port_msg_notfind))

    else:
        print("仅支持 windows")


if __name__ == "__main__":
    port_stop()
