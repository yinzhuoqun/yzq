# -*- coding: utf-8 -*-
import random, time

big = ['b', 'B', 'big', 'Big']
small = ['s', 'S', 'small', 'Small']


def roll_dice(number=3, points=None):
    print('Choice points...')
    if points is None:
        points = []
    while number > 0:
        point = random.randrange(1, 7)
        points.append(point)
        number = number - 1
    return points


def roll_result(total):
    if 11 <= total <= 18:
        # win
        return True
    else:
        # elif 3 <= total <= 10:
        # fail
        return False


def start_game():
    your_money = 1000
    # your_bet = 0
    print('Game Start：1~10 points is small if you win bet*2 >>>')
    print('your money: %s' % your_money)
    while your_money > 0:

        choices = big + small
        your_choices = input('big / small:\n')

        if your_choices in choices:
            your_bet = input('How much you wanna bet?\n')
            your_bet_range = [str(x) for x in range(1, your_money + 1)]
            while your_bet not in your_bet_range:
                if your_bet.isdigit():
                    if int(your_bet) > your_money:
                        print('Your money less then your bet.')
                    if int(your_bet) == 0:
                        print('Your bet is None.')
                else:
                    print('Your input is wrong.')
                your_bet = input('How much you wanna bet?\n')

            your_bet = int(your_bet)
            points = roll_dice()
            youWin = roll_result(sum(points))
            # print(youWin, your_choices, roll_result(total), total)
            if youWin and your_choices in big:
                print('The points is {}'.format(points), 'You win!')
                your_money = your_money + your_bet * 2
                print('You gained {}, you have {} now'.format(your_bet, your_money))

            elif youWin == False and your_choices in small:
                print('The points is {}'.format(points), 'You win!')
                your_money = your_money + your_bet * 2
                print('You gained {}, you have {} now'.format(your_bet, your_money))

            else:
                print('The points is ', points, 'You lost!')
                your_money = your_money - your_bet
                print('You lost {}, you have {} now'.format(your_bet, your_money))

        else:
            print('Please, choice your size?')
        print("Money:", your_money)
    else:
        print('Game Over')
        time.sleep(3)


def findme(strme, strs):
    y = -1
    strme_pos = []
    for x in strs:
        y += 1
        for z in strme:
            if x == z:
                print(x)

        print(x, y)


if __name__ == "__main__":
    # start_game()

    strme = "abc"
    strs = "dagadsfasdabcdfadgadabcdafasdfaabc"
    findme(strme, strs)
