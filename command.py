#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'yinzhuoqun'

# 日志级别等级 ERROR > WARNING > INFO > DEBUG 等几个级别
import logging

logging.basicConfig(level=logging.INFO)
# logging.basicConfig(level=logging.WARNING)

import re, os, time, subprocess


class path(object):
    pass


class device(object):
    def __init__(self, devicename):
        self.devicename = devicename

    def adb_install(self, path):


        pass

    def adb_uninstall(self, packagename):
        pass

    def adb_clear(self, packagename):
        pass

    def adb_printscreen(self, devicename):
        pass

    def adb_connect(self, devicename):
        '''
        your device must be open root permission
        :param devicename:
        :return:
        '''
        pass

    def adb_monkey(self, packagename):
        pass


class package(object):
    def __init__(self):
        self.packagename = packagename
        self.startactivity = startactivity

    def package_dump(self, path):
        aapt = 1
        try:
            PAInfo = subprocess.check_output('aapt dump badging %s' % path).decode('gbk', 'ignore')
        except Exception as e:

            where_adb = subprocess.check_output('where adb').decode('gbk', 'ignore');  # print(where_adb)
            where_aapt = subprocess.check_output('where aapt').decode('gbk', 'ignore');  # print(where_aapt)

            adb_env_list = re.findall('adb.exe', where_adb);  # print(adb_env_list)
            aapt_env_list = re.findall('aapt.exe', where_aapt);  # print(aapt_env_list)

            if len(adb_env_list) == 0:
                print('错误：请添加 adb.exe 的环境变量')
            if len(aapt_env_list) == 0:
                print('错误：请添加 aapt.exe 的环境变量')
            else:
                print('错误：无效的apk文件')

            aapt = 0
            return None

        if aapt == 1:
            PList = re.findall(r"name='(.+?)' versionCode='", PAInfo);  # print('Plist',PList)
            Alist = re.findall(r"launchable-activity: name='(.+?)'  label='", PAInfo);  # print('Alist',Alist)
            if len(PList) != 0:
                if len(Alist) != 0:
                    PAList = PList + Alist;  # print(PAList)
                    return PAList
                else:
                    print('警告：导出启动的 Activity 失败')
                    return PList;  # print(PList)

            else:
                print('警告：导出包名、启动的 Activity 失败')
                return None

    def package_monkey(self):
        pass
