#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'yinzhuoqun'

# 日志级别等级 ERROR > WARNING > INFO > DEBUG 等几个级别
import logging

logging.basicConfig(level=logging.INFO)


# logging.basicConfig(level=logging.WARNING)

# 调整堆
def adjust_heap(lists, i, size):
    '''
    :param lists: 要排序的列表
    :param i: 0 到 size/2 的 list 的元素
    :param size: list lenth
    :return:
    '''
    lchild = 2 * i + 1
    rchild = 2 * i + 2
    max = i
    if i < size / 2:
        if lchild < size and lists[lchild] > lists[max]:
            max = lchild
        if rchild < size and lists[rchild] > lists[max]:
            max = rchild
        if max != i:
            lists[max], lists[i] = lists[i], lists[max]
            adjust_heap(lists, max, size)

# 生成堆
def build_heap(lists, size):
    for i in range(0, int(size / 2))[::-1]:  # range 范围应该是整数
        adjust_heap(lists, i, size)

# 堆排序
def heap_sort(lists):
    size = len(lists)
    build_heap(lists, size)
    for i in range(0, size)[::-1]:
        lists[0], lists[i] = lists[i], lists[0]
        adjust_heap(lists, 0, i)
        print(lists)


def get_mydata(str):
    import re
    reg = r'Code:(\w+)'
    datareg = re.compile(reg, re.M)  #  re.M多行匹配，影响 ^ 和 $
    mydatas = re.findall(datareg, str)
    print(mydatas)
    return mydatas


if __name__ == "__main__":
    import random
    mylist = [56, 6, 4, 5, 32, 7, 8, 4, 2, 1, 0]
    random.shuffle(mylist)
    print("random:",mylist)
    heap_sort(mylist)

    str= "’Code:12313132’’Code:12313132‘’Code:12313132‘’Code:12313132‘’" \
         "Code:12313132‘’Code:12313132‘‘A:123123’‘A:123123’‘A:123123’‘b:qqqqq’" \
         "‘b:qqqqq’‘b:qqqqq’‘b:qqqqq’ ’Code:12313132’’Code:12313132‘’Code:12313132‘" \
         "’Code:12313132‘’Code:12313132‘’Code:12313132‘‘A:123123’‘A:123123’‘A:123123’" \
         "‘b:qqqqq’‘b:qqqqq’‘b:qqqqq’‘b:qqqqq’ ’Code:12313132’" \
         "’Code:12313132‘’Code:12313132‘’Code:12313132‘’Code:12313132‘’Code:12313132‘" \
         "‘A:123123’‘A:123123’‘A:123123’‘b:qqqqq’‘b:qqqqq’‘b:qqqqq’‘b:qqqqq’"

    # get_mydata(str)
