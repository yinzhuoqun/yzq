#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'yinzhuoqun'

# 日志级别等级 ERROR > WARNING > INFO > DEBUG 等几个级别
import logging

logging.basicConfig(level=logging.INFO)
# logging.basicConfig(level=logging.WARNING)

import warnings
warnings.filterwarnings("ignore")

import jieba  # 分词包
import numpy  # numpy计算包
# import codecs  # codecs提供的open方法来指定打开的文件的语言编码，它会在读取的时候自动转换为内部unicode
import re, os, random
import pandas as pd
import matplotlib.pyplot as plt
from urllib import request
from bs4 import BeautifulSoup as bs
# %matplotlib inline  # ipython
import matplotlib

matplotlib.rcParams['figure.figsize'] = (10.0, 5.0)
from wordcloud import WordCloud  # 词云包

# 友好访问
data = None
agent_list = [
    'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0']
headers = {
    # 'Referer': '%s' % ref,
    'User-Agent': '%s' % random.choice(agent_list)
}

# 分析网页函数
def getNowPlayingMovie_list():
    url = "https://movie.douban.com/nowplaying/hangzhou/"
    req = request.Request(url, data=data, headers=headers)
    resp = request.urlopen(req)
    html_data = resp.read().decode('utf-8')
    soup = bs(html_data, 'html.parser')
    nowplaying_movie = soup.find_all('div', id='nowplaying')
    # nowplaying_movie_list = nowplaying_movie[0].find_all('li', class_='list-item')  # 获取第一个电影
    nowplaying_movie_list = nowplaying_movie[0].find_all('li', class_='list-item')  # 获取第一个电影
    nowplaying_list = []
    for item in nowplaying_movie_list:
        nowplaying_dict = {}
        nowplaying_dict['id'] = item['data-subject']
        for tag_img_item in item.find_all('img'):
            nowplaying_dict['name'] = tag_img_item['alt']
            nowplaying_list.append(nowplaying_dict)
    print("线上电影名称：%s" % [movie["name"] for movie in nowplaying_list])
    return nowplaying_list


# 爬取评论函数
def getCommentsById(movieId, pageNum):
    eachCommentList = []
    if pageNum > 0:
        start = (pageNum - 1) * 20
    else:
        return False
    requrl = 'https://movie.douban.com/subject/' + movieId + '/comments' + '?' + 'start=' + str(start) + '&limit=20'
    # print(requrl)

    req = request.Request(requrl, data, headers)
    resp = request.urlopen(req)
    html_data = resp.read().decode('utf-8')
    soup = bs(html_data, 'html.parser')
    comment_div_lits = soup.find_all('div', class_='comment')
    for item in comment_div_lits:
        if item.find_all('p')[0].string is not None:
            eachCommentList.append(item.find_all('p')[0].string)
    return eachCommentList


def main(movie_name="战狼2"):
    app_list = [name for name in os.listdir("movie_txt") if os.path.isfile(os.path.join("movie_txt", name))]
    if app_list:
        print("本地评语库：%s" % app_list)
    for name in app_list:
        if movie_name in name:
            movie_name = name
    movie_txt_path = "movie_txt/%s" % movie_name
    if os.path.exists(movie_txt_path) == False:
        # 循环获取第一个电影的前10页评论
        commentList = []
        NowPlayingMovie_list = getNowPlayingMovie_list()
        # spider_movie_id = NowPlayingMovie_list[0]['id']  # 正在上映的第一个电影
        spider_movie_id_list = [movie for movie in NowPlayingMovie_list if movie_name in movie["name"]]
        if spider_movie_id_list:
            spider_movie_id = spider_movie_id_list[0]["id"]
            spider_movie_name = spider_movie_id_list[0]["name"]
        else:
            spider_movie_id = NowPlayingMovie_list[0]['id']  # 默认正在上映的第一个电影
            spider_movie_name = NowPlayingMovie_list[0]["name"]
        print("电影全名：%s" % spider_movie_name)

        for i in range(5):
            num = i + 1
            commentList_temp = getCommentsById(spider_movie_id, num)
            commentList.append(commentList_temp)

        # 将列表中的数据转换为字符串
        comments = ''
        for k in range(len(commentList)):
            comments = comments + (str(commentList[k])).strip()

        # 使用正则表达式去除标点符号
        pattern = re.compile(r'[\u4e00-\u9fa5]+')
        filterdata = re.findall(pattern, comments)
        cleaned_comments = ''.join(filterdata)
        # print(cleaned_comments)

        # 将干净的评论存入 txt
        with open("movie_txt/%s.txt" % spider_movie_name, "w") as f:
            f.write(cleaned_comments)
    else:
        # 将存入的干净的评论从 txt 取出
        with open(movie_txt_path, "r") as f:
            cleaned_comments = f.read()
            # print(cleaned_comments)

    # 使用结巴分词进行中文分词
    segment = jieba.lcut(cleaned_comments)
    words_df = pd.DataFrame({'segment': segment})

    # 去掉停用词
    stopwords = pd.read_csv("stopwords.txt", index_col=False, quoting=3, sep="\t", names=['stopword'],
                            encoding='utf-8')  # quoting=3全不引用
    words_df = words_df[~words_df.segment.isin(stopwords.stopword)]

    # 统计词频
    words_stat = words_df.groupby(by=['segment'])['segment'].agg({"计数": numpy.size})
    words_stat = words_stat.reset_index().sort_values(by=["计数"], ascending=False)

    # 用词云进行显示
    # wordcloud = WordCloud(font_path="simhei.ttf", background_color="white", max_font_size=80)  # 指定字体类型、字体大小和字体颜色
    wordcloud = WordCloud(font_path="font_ttf/simhei.ttf", background_color="white", max_font_size=80)  # 指定字体类型、字体大小和字体颜色
    word_frequence = {x[0]: x[1] for x in words_stat.head(1000).values}

    word_frequence_list = []
    for key in word_frequence:
        temp = (key, word_frequence[key])
        word_frequence_list.append(temp)
    # print(word_frequence_list[0:10])

    wordcloud = wordcloud.fit_words(dict(word_frequence_list))

    plt.imshow(wordcloud)
    plt.axis("off")  # 坐标轴范围
    plt.title(movie_name)
    plt.show()  # 展示


if __name__ == "__main__":
    # 主函数
    print("影评地址：https://movie.douban.com/cinema/nowplaying/hangzhou/")
    main(input('请输入电影名称(不完全的影名也可以)：'))
    # main("战狼2")
