#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'yinzhuoqun'

# 日志级别等级 ERROR > WARNING > INFO > DEBUG 等几个级别
import logging

logging.basicConfig(level=logging.INFO)
# logging.basicConfig(level=logging.WARNING)


import re, os, time
import requests


# post
def http_post(url, body):
    # r = requests.post(url, data=json.dumps(body))
    """

    :rtype :
    """
    r = requests.post(url, data=body)
    # print(r.json())
    # print(r.headers['content-type'])
    # print(r.status_code)
    # print(r.encoding)
    # print(r.text)
    return r.text


# get
def http_get(url, hearder):
    r = requests.get(url, headers=hearder)
    # print(r.json())
    # print(r.headers['content-type'])
    # print(r.status_code)
    # print(r.encoding)
    b = r.text.encode()
    # print(type(r.text))
    # print(b.decode())
    # info=r.text.encode('GBK','ignore')
    # info1=info.decode('utf-8','ignore');print(info1)
    # print(type(info))
    # print(r.text.encode('GBK','ignore'))
    return r.text


'''
url_ticket = r'http://192.168.199.126:8080/account/basic/ticket'#外网
url_info = r'http://192.168.199.126:8080/space/info/viewall?long_no=%s'%id #外网

url_ticket = r'http://192.168.2.175:8080/account/basic/ticket'#内网
url_info = r'http://192.168.2.175:8080/space/info/viewall?long_no=%s'%id #内网
'''


def get_ticket_info(url_ticket, url_info, id, password):
    # url_ticket=r'http://192.168.199.126:8080/account/basic/ticket'#外网
    body_ticket = u'username=%s&password=%s' % (id, password)  # 床号，密码

    try:
        post_info = http_post(url_ticket, body_ticket)
        p = re.compile(r'ticket":"(.*?)"},"code')
        ticket_list = re.findall(p, post_info)
        # print('ticket =', ticket_list)
        body_info = {"ticket": '%s' % ticket_list[0]}

        return True
    except Exception as e:
        print('获取用户 ticket 信息失败:')

        return False

        # 用ticket获取信息
        # url_info=r'http://192.168.199.126:8080/space/info/viewall?long_no=%s'%id #外网

        # try:
        #     get_info = http_get(url_info, body_info)
        #     account_id_list = re.findall('account_id":(.*?),"constellation":"', get_info)
        #     print('account_id =', account_id_list)  # account_id
        #     info_name = '用户信息%s.txt' % id
        #
        #     try:
        #         with open(info_name, 'wb') as f:
        #             f.write(get_info.encode("utf-8"))
        #         info_file_path = os.path.join(os.getcwd(), info_name)
        #     except Exception as e:
        #         print("用户信息写入文件失败")
        #     else:
        #         if os.path.exists(info_file_path):
        #             print('用户信息保存在：%s' % info_file_path)
        #             # print(get_info) #全部信息
        #
        #
        # except Exception as e:
        #     print('获取用户信息失败:')


def input_my():
    id = input('请输入龙号：').strip()
    while id.isdigit() == False:
        if len(id) == 0:
            print('!账号不能为空')
        if id.isdigit() == False:
            print('!输入的龙号有误')
        id = input('请输入龙号：')

    password = input('请输入密码：')
    while len(password) == 0:
        print("!密码不能为空")
        password = input('请输入密码：')

    return id, password


def get_info_in():
    # if get_model.lower() in ["i","0"]:
    print('>>> 内网模式')
    id, password = input_my()
    url_ticket = r'http://192.168.2.175:8080/account/basic/ticket'  # 内网
    url_info = r'http://192.168.2.175:8080/space/info/viewall?long_no=%s' % id  # 内网
    status = get_ticket_info(url_ticket, url_info, id, password)

    return status


def get_info_out(id, password):
    # elif get_model.lower() in ["0","1"]:
    print('>>> 外网模式')
    # id, password = input_my()
    url_ticket = r'http://192.168.199.126:8080/account/basic/ticket'  # 外网
    url_info = r'http://192.168.199.126:8080/space/info/viewall?long_no=%s' % id  # 外网
    status = get_ticket_info(url_ticket, url_info, id, password)
    # print(status)
    return status


if __name__ == "__main__":
    with open(r"C:\Users\lifeix\Desktop\testin_id.txt") as f:
        text = f.readlines()
        # print(text)
        for x in text:
            x = x.split(",")
            # print(x)
            id = x[0]
            pwd = x[1].replace("\n", "")
            print("-" * 20)
            print(id, pwd)
            if get_info_out(id, pwd):
                print("账号密码正确")
            else:
                print("账号不对应")
