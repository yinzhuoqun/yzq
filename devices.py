#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'yinzhuoqun'

# 日志级别等级 ERROR > WARNING > INFO > DEBUG 等几个级别
import logging

logging.basicConfig(level=logging.INFO)
# logging.basicConfig(level=logging.WARNING)


import os, re

# 获取设备列表函数
def devices():
    """
    get android devices name list
    tips:
    you require install adb environment

    :return:
    """

    try:
        devices_info = os.popen('adb devices')
    except Exception as e:
        print(e)
    finally:
        devices_info = devices_info.read()
        devices_list = re.findall(r'(.*?)\tdevice\b', devices_info)

    return devices_list


if __name__ == "__main__":
    logging.info(devices())
