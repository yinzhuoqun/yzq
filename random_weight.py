#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'yinzhuoqun'

# 日志级别等级 ERROR > WARNING > INFO > DEBUG 等几个级别
import logging

logging.basicConfig(level=logging.INFO)
# logging.basicConfig(level=logging.WARNING)


import random
import bisect  # 排序模块

list = ['谢谢参与', '三等奖', '二等奖', '一等奖']


def weight_choice(weight):
    """
    :param weight: list对应的权重序列
    :return:选取的值在原列表里的索引
    """
    weight_sum = []
    sum = 0
    for a in weight:
        sum += a
        weight_sum.append(sum)

    t = random.randint(0, sum - 1)
    print("t", t)
    print("sum", sum)
    print("w_sum", weight_sum)
    pos = bisect.bisect_right(weight_sum, t)
    print("pos", pos)
    # return bisect.bisect_right(weight_sum, t)
    return pos


if __name__ == "__main__":
    for x in range(1):
        print(list[weight_choice([5, 2, 2, 1])])
