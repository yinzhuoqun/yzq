#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'yinzhuoqun'

'''
有规律（不含空格）：
全角字符unicode编码从65281~65374 （十六进制 0xFF01 ~ 0xFF5E）
半角字符unicode编码从33~126 （十六进制 0x21~ 0x7E）

特例：
空格比较特殊，全角为 12288（0x3000），半角为 32（0x20）

除空格外，全角/半角按unicode编码排序在顺序上是对应的（半角 + 0x7e= 全角）,所以可以直接通过用+-法来处理非空格数据，对空格单独处理。
chr()函数用一个范围在range（256）内的（就是0～255）整数作参数，返回一个对应的字符。
ord()函数是chr()函数（对于8位的ASCII字符串）或unichr()函数（对于Unicode对象）的配对函数，它以一个字符（长度为1的字符串）作为参数，返回对应的ASCII数值，或者Unicode数值。
'''

import os, time


def strQ2B(ustring):
    """全角转半角"""
    rstring = ""
    for uchar in ustring:
        inside_code = ord(uchar)
        # print(inside_code)
        if inside_code == 12288:  # 全角空格直接转换
            inside_code = 32
            rstring += chr(inside_code)
        elif inside_code >= 65281 and inside_code <= 65374:  # 全角字符（除空格）根据关系转化
            inside_code -= 65248
            # inside_code += 32
            # print(inside_code)
            rstring += chr(inside_code)
            rstring += chr(32)  # 加半角空格
        else:
            rstring += chr(inside_code)
    return rstring


def strB2Q(ustring):
    """半角转全角"""
    rstring = ""
    for uchar in ustring:
        inside_code = ord(uchar)
        if inside_code == 32:  # 半角空格直接转化
            inside_code = 12288
        elif inside_code >= 32 and inside_code <= 126:  # 半角字符（除空格）根据关系转化
            inside_code += 65248
        rstring += chr(inside_code)
    return rstring


if __name__ == "__main__":
    # mstring = "领取鲜花种子后每日浇水可获得15点成长值；好友间可以开启“顺手牵羊”模式收取好友植物的成长值，也有可能收取失败扣除成长值；"
    while 1:
        mstring = input("请输入要转成半角字符的文字：\n")
        # mstring = "当鲜花的成长总值首次达到1200时获得劳模奖章，鲜花的成长总值达到3500时获得为人民服务大厅背景；鲜花的成长总值达到8000时获得花匠小能手的大厅背景（背景持续至5月9日12：00）。"
        b = strQ2B(mstring)
        print("全角转半角字符：")
        former = "原文：\n%s" % mstring
        # print(former)
        result = "结果：\n%s" % b
        print(result)

        filename = "full_to_half.txt"
        with open(filename, "a+") as f:
            f.write(time.strftime("%Y-%m-%d %H:%M:%S") + "\n" + former + "\n" + result + "\n\n")
        print("结果保存在：%s" % os.path.join(os.getcwd(), filename))
        print("半角转全角字符：")
        istring = "当鲜花的成长总值首次达到1200时获得劳模奖章,鲜花的成长总值达到3500时获得为人民服务大厅背景;鲜花的成长总值达到8000时获得花匠小能手的大厅背景(背景持续至5月9日12:00)。"
        istring = input("请输入要转成全角字符的文字：\n")
        c = strB2Q(istring)
        print("原文：%s" % istring)
        print("结果：%s" % c)
